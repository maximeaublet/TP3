// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var Hashes = require('jshashes');

var SHA256 =  new Hashes.SHA256
var SHA512 = new Hashes.SHA512;

app.use(cookieParser());

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	//Header req
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();

});

// Serve static files
app.use("/static", express.static('public'));


app.get("/", function(req, res, next) {

	if(req.cookies.token){

		db.all("SELECT ident FROM sessions WHERE token = ?", [req.cookies.token] ,function(err, data){
			if(data.length >= 1){
				res.sendfile("public/welcome.html");
			}else{
				res.sendfile("public/index.html");
			}
		});
	}else{
		res.sendfile("public/index.html");
	}
	
});


app.get("/logout", function(req, res, next) {

	db.all("DELETE FROM WHERE token = ?", [req.cookies.token],function(err, data){
		res.clearCookie("token");
		res.redirect("/");
	});
	
});



app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next){

	db.all("SELECT rowid FROM users WHERE ident = ? AND password = ?", [req.body.login, req.body.password],function(err, data){
		if(data.length >= 1){

			//Un vrai hash bien aléatoire
			var token =  SHA512.hex(SHA256.hex(Math.random()) + SHA512.hex(req.body.login) + SHA512.hex(req.body.password) + SHA256.hex(Date.now()));

			db.all("SELECT token FROM sessions WHERE ident = ?", [req.body.login] ,function(err, data){
				if(data.length >= 1){

					db.all("UPDATE sessions SET token = ? WHERE ident = ?", [token, req.body.login], function (err, data){
						res.cookie('token', token).redirect("/");
					});

				}else{
					db.all("INSERT INTO sessions (ident, token) VALUES (?, ?)", [req.body.login, token], function (err, data){
						res.cookie('token', token).redirect("/");
					});
				}
			});

		}else{
			res.redirect("/");
		}
    });
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
