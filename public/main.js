function btn_click(){

	//Definitions
	var string = document.getElementById("rech").value;
	var url = "http://localhost:1234/api?action=opensearch&search=" + string + "&format=json";
	var xhr = new XMLHttpRequest();
	var json_resp = '';
	var list_resp = '<br />';
	

	//Configuration de la reponse
	xhr.onreadystatechange = function(){
    	if (xhr.readyState==4 && xhr.status==200)
    	{
    		//Obtention reponse + parse
    		resp = xhr.responseText;
    		json_resp = JSON.parse(resp);

    		//Mise en forme resultat
     		for(i=0; i<json_resp[1].length; i++ ){
     			list_resp += '<li class="li_resp">' + json_resp[1][i] + '</li>';
     		}

     		//Affichage dans #res
			document.getElementById("res").innerHTML = list_resp;
	    }
	}
	xhr.open("GET", url, true);
	
	//Lancement
	xhr.send();

}